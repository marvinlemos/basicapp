/****************************************************************************
 *  Copyright: National ICT Australia,  2007 - 2011                         *
 *  Developed at the ATP lab, Networked Systems research theme              *
 *  Author(s): Athanassios Boulis, Yuriy Tselishchev                        *
 *  This file is distributed under the terms in the attached LICENSE file.  *
 *  If you do not find this file, copies can be found by writing to:        *
 *                                                                          *
 *      NICTA, Locked Bag 9013, Alexandria, NSW 1435, Australia             *
 *      Attention:  License Inquiry.                                        *
 *                                                                          *
 ****************************************************************************/

#include "BasicApp.h"

Define_Module(BasicApp);

void BasicApp::startup()
{
	sampleInterval = (double)par("sampleInterval") / 1000;
	quantSamples = (int) par("quantSamples");
	count = 0;
	aggregatedValue = 0.0;
	lastSensedValue = 0.0;
	totalPackets = 0;
	currSentSampleSN = 0;

	latencyMax = hasPar("latencyHistogramMax") ? par("latencyHistogramMax") : 100;
	latencyBuckets = hasPar("latencyHistogramBuckets") ? par("latencyHistogramBuckets") : 10;

	if (! isSink)
		setTimer(REQUEST_SAMPLE, 0);

	declareOutput("Packets received per node");
	declareHistogram("Samples per node", 0, latencyMax, latencyBuckets);
}

void BasicApp::fromNetworkLayer(ApplicationPacket * rcvPacket,
		const char *source, double rssi, double lqi)
{
	int sequenceNumber = rcvPacket->getSequenceNumber();
	BasicPacket *basicPacket = check_and_cast<BasicPacket*>(rcvPacket);

	if (isSink){
		trace() << "Sink received from: " << source << " \tvalue=" << basicPacket->getData();
		collectOutput("Packets received per node", source);
		//collectHistogram("Samples per node", self, 0);
	}
}

void BasicApp::timerFiredCallback(int index)
{
	switch (index) {
		case REQUEST_SAMPLE:{
			requestSensorReading();
			setTimer(REQUEST_SAMPLE, sampleInterval);
			break;
		}
	}
}


void BasicApp::handleSensorReading(SensorReadingMessage * rcvReading)
{
	string sensType(rcvReading->getSensorType());
	double sensValue = rcvReading->getSensedValue();
	lastSensedValue = sensValue;
	count++;

	// schedule the TX of the value
	trace() << "Sensed = " << sensValue;
	//collectHistogram("Samples per node", self, sensValue);

	aggregatedValue = aggregatedValue + lastSensedValue;

	if (count > quantSamples-1){
		aggregatedValue = aggregatedValue / quantSamples;

		BasicPacket* basicPacket = new BasicPacket("Value Reporting Packet", APPLICATION_PACKET);
		basicPacket->setNodeId((unsigned short)self);
		basicPacket->setData(aggregatedValue);

		basicPacket->setSequenceNumber(currSentSampleSN);
		currSentSampleSN++;

		toNetworkLayer(basicPacket, SINK_NETWORK_ADDRESS);
		count = 0;
	}
}

