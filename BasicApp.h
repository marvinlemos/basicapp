/****************************************************************************
 *  Copyright: National ICT Australia,  2007 - 2010                         *
 *  Developed at the ATP lab, Networked Systems research theme              *
 *  Author(s): Athanassios Boulis, Yuriy Tselishchev                        *
 *  This file is distributed under the terms in the attached LICENSE file.  *
 *  If you do not find this file, copies can be found by writing to:        *
 *                                                                          *
 *      NICTA, Locked Bag 9013, Alexandria, NSW 1435, Australia             *
 *      Attention:  License Inquiry.                                        *
 *                                                                          *
 ****************************************************************************/

#ifndef _BasicApp_H_
#define _BasicApp_H_

#include "VirtualApplication.h"
 #include "BasicPacket_m.h"

using namespace std;

enum BasicAppTimers {
	REQUEST_SAMPLE = 1,
	SEND_AGGREGATED_VALUE = 2
};

class BasicApp: public VirtualApplication {
 private:
	int quantSamples;
	int count;
	double aggregatedValue;
	double sampleInterval;
	int currSentSampleSN;
	double latencyMax;
	int latencyBuckets;

	double lastSensedValue;
	double totalPackets;

 protected:
	void startup();
	void fromNetworkLayer(ApplicationPacket *, const char *, double, double);
	void timerFiredCallback(int);
	void handleSensorReading(SensorReadingMessage * rcvReading);
};

#endif				// _BasicApp_APPLICATIONMODULE_H_
